package novako19bot;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.Weight;
import robocode.DeathEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

import java.io.IOException;

public class GeneticBot extends Robot {

    public static final double ENERGY_DIV = 100.0;

    public static final String[] inputs = new String[] {
            "posx", "posy", "heading", "gunHeading", "gunReady", "energy",                  // robot
            "scDistance", "scVelocity", "scHeading", "scEnergy", "scAge",                   // scanned
            "numOtherRobots"                                                                // other
    };

    public static final String[] outputs = new String[] {
            "move", "fire", "turnRobot", "turnGun"
    };

    private Integer id = null;
    private NetConfig config;

    private boolean isAlive = true;
    private double enemyDist;
    private double enemyVel;
    private double enemyEnergy;
    private double enemyHeading;
    private long enemyScanTime;

    @Override
    public void run() {
        if(!this.init()) {
            System.out.println("Couldn't init");
        }
        this.loop();

        System.out.println("Robot died.");
    }

    public boolean init() {
        try {
            this.config = NetConfig.load(this.getRobotId());
        } catch (IOException e) {
            System.out.println(e);
            return false;
        } catch (ClassNotFoundException e) {
            System.out.println(e);
            return false;
        }

        String[] netsLoaded = this.config.keySet().toArray(new String[this.config.size()]);
        System.out.println("Nets loaded: " + String.join(", ", netsLoaded));

        return true;
    }

    public void loop() {
        while(true)
        {
            if(!this.isAlive) return;

            double[] inputVec = this.constructInputVec();
            String action = this.decideAction(inputVec);
            System.out.println(action);
            this.doAction(action, inputVec);
        }
    }

    private double[] constructInputVec() {
        // construct input vector
        double[] inputVec = new double[inputs.length];
        for(int i = 0; i < inputVec.length; i++) {
            inputVec[i] = this.readInput(inputs[i]);
        }

        return inputVec;
    }

    private String decideAction(double[] inputVec) {
        NeuralNetwork nn = this.config.get("main");

        // set input, calculate the net and get output
        nn.setInput(inputVec);
        nn.calculate();
        double[] outputVec = nn.getOutput();

        System.out.print("Output vec: ");
        for(int i = 0; i < outputVec.length; i++) {
            System.out.print(outputVec[i] + " ");
        }
        System.out.println();

        // find the index of the output with max. value
        double max = Double.MIN_VALUE;
        int maxi = 0;

        for(int i = 0; i < outputVec.length; i++) {
            if(outputVec[i] > max) {
                max = outputVec[i];
                maxi = i;
            }
        }

        // and return name of the action to do
        return outputs[maxi];
    }

    private void doAction(String action, double[] inputVec) {
        NeuralNetwork nn = this.config.get(action);

        // set input, calculate the net and get output
        nn.setInput(inputVec);
        nn.calculate();
        double[] outputVec = nn.getOutput();

        this.writeOutput(action, outputVec);
    }

    public double readInput(String in) {
        switch(in) {
            case "posx":
                return this.getX();
            case "posy":
                return this.getY();
            case "heading":
                return this.getHeadingNormalized();
            case "gunHeading":
                return this.getGunHeadingNormalized();
            case "gunReady":
                return this.getGunHeat() == 0 ? 1.0 : 0.0;
            case "energy":
                return this.getEnergy();
            case "scDistance":
                return this.enemyDist;
            case "scHeading":
                return this.enemyHeading;
            case "scVelocity":
                return this.enemyVel;
            case "scEnergy":
                return this.enemyEnergy;
            case "scAge":
                return this.getTime() - this.enemyScanTime;
            case "numOtherRobots":
                return this.getOthers();
            default:
                throw new UnsupportedOperationException("Unknown input " + in);
        }
    }

    public void writeOutput(String out, double... val) {
        switch(out) {
            case "move":
                this.ahead(val[0]);
                break;
            case "fire":
                this.fire(val[0]);
                break;
            case "turnRobot":
                this.turnRight(val[0] * 180);
                break;
            case "turnGun":
                this.turnGunRight(val[0] * 180);
                break;
            default:
                throw new UnsupportedOperationException("Unknown output " + out);
        }
    }

    private double getHeadingNormalized() {
        double centered = this.getHeading() - 180;
        return centered;
    }

    private double getGunHeadingNormalized() {
        double centered = this.getGunHeading() - 180;
        return centered;
    }


    private int getRobotId() {
        if(this.id != null) return this.id;

        if(!this.getName().endsWith(")")){
            this.id = 0;
        }
        else {
            // name is in format "robot (id)"
            // first reverse the name and get position of ( char
            String rev = new StringBuilder(this.getName()).reverse().toString();
            int bracket = rev.indexOf("(");

            // get the id, reverse it back and parse it to int
            String id = new StringBuilder(rev.substring(1, bracket)).reverse().toString();
            this.id = Integer.parseInt(id);
        }

        return this.id;
    }

    @Override
    public void onDeath(DeathEvent event) {
        super.onDeath(event);
        this.isAlive = false;
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent event) {
        this.enemyDist = event.getDistance(); 
        this.enemyVel = event.getVelocity();
        this.enemyEnergy = event.getEnergy();
        this.enemyHeading = event.getHeading();
        this.enemyScanTime = this.getTime();
    }
}
