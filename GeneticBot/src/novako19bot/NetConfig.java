package novako19bot;

import org.neuroph.core.NeuralNetwork;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;
import java.util.HashMap;

public class NetConfig extends HashMap<String, NeuralNetwork> {

    private static String COMM_FILES_PATH = "/run/user/1000/robocode.%d.comm";

    public NetConfig reproduce(NetConfig c) {
        throw new NotImplementedException();
    }

    public NetConfig reproduce() {
        throw new NotImplementedException();
    }

    public static NetConfig load(int id) throws IOException, ClassNotFoundException {
        String path = String.format(COMM_FILES_PATH, id);

        FileInputStream fis = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStream(fis);

        return (NetConfig) ois.readObject();
    }

    public static void save(int id, NetConfig c) throws IOException {
        String path = String.format(COMM_FILES_PATH, id);

        FileOutputStream fileOut = new FileOutputStream(path);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(c);
    }
}
