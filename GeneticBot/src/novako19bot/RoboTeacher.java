package novako19bot;


import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.Neuron;
import org.neuroph.core.Weight;
import org.neuroph.core.transfer.Sigmoid;
import org.neuroph.core.transfer.TransferFunction;
import org.neuroph.nnet.MultiLayerPerceptron;
import robocode.control.BattleSpecification;
import robocode.control.BattlefieldSpecification;
import robocode.control.RobocodeEngine;
import robocode.control.RobotSpecification;

import java.io.IOException;
import java.util.*;

public class RoboTeacher {

    private static int NUM_GENERATIONS = 1000;
    private static int NUM_ROUNDS = 1;


    private final RobocodeEngine engine;
    private final BattleSpecification battleSpec;
    private final BattleObserver observer;

    private int numRobots;
    private NetConfig[] configurations;

    public RoboTeacher(int numRobots) {
        this.numRobots = numRobots;

        this.configurations = new NetConfig[this.numRobots];

        // robocode init
        // Disable log messages from Robocode
        RobocodeEngine.setLogMessagesEnabled(false);

        // Create the RobocodeEngine
        this.engine = new RobocodeEngine();

        // Add our own battle listener to the RobocodeEngine
        this.observer = new BattleObserver();
        this.engine.addBattleListener(this.observer);

        // Show the Robocode battle view
//        this.engine.setVisible(true);

        // Setup the battle specification
        BattlefieldSpecification battlefield = new BattlefieldSpecification(800, 600); // 800x600

        // Add 'numRobots' GeneticBots to the match
        RobotSpecification[] selectedRobots = engine.getLocalRepository(String.join(",", Collections.nCopies(this.numRobots, "novako19bot.GeneticBot*")));

        // Prepare battlefield
        this.battleSpec = new BattleSpecification(NUM_ROUNDS, battlefield, selectedRobots);
    }

    public void teach() {
        for(int i = 0; i < this.numRobots; i++) {
            this.configurations[i] = generateConfig();
        }

        try {
            this.writeConfigurations();
        } catch (IOException e) {
            System.out.println("Couldn't write the comm file: " + e.getMessage());
            return;
        }

        System.out.println("Quitting as requested");
        System.exit(0);

        for (int gen = 0; gen < NUM_GENERATIONS; gen++) {
            int score;

            this.refreshNetworks();
            score = this.runSim();

            System.out.println(String.format("Generation {0}, score {1}.", gen, score));
        }
    }

    private void writeConfigurations() throws IOException {
        for(int i = 0; i < this.numRobots; i++) {
            NetConfig.save(i+1, this.configurations[i]);
        }
    }

    private static NetConfig generateConfig() {
        int[] defaultLayers = new int[] {20, 20};
        TransferFunction[] defaultTF = Collections.nCopies(5, new Sigmoid()).toArray(new TransferFunction[5]);

        NetConfig newNet = new NetConfig();

        // generate action-deciding NN with different actions as an output
        NeuralNetwork main = initNN(
                GeneticBot.inputs.length,
                GeneticBot.outputs.length,
                defaultLayers,
                defaultTF

        );
        newNet.put("main", main);

        // generate multiple activeConfiguration to generate arguments of selected action
        for (String action : GeneticBot.outputs) {
            NeuralNetwork n = initNN(
                    GeneticBot.inputs.length,
                    1,
                    defaultLayers,
                    defaultTF
            );
            newNet.put(action, n);
        }

        return newNet;
    }

    /**
     * Initializes and returns a neural network.
     * @param numInputs Number of input features.
     * @param numOutputs Number of outputs.
     * @param numInHiddenLayers An array of neuron count in each hidden layer. Length of the array gives the number of hidden layers.
     * @param transferFunctions An array of TransferFunctions for each layer but input layer.
     * @return Initialized neural network.
     */
    private static NeuralNetwork initNN(int numInputs, int numOutputs, int[] numInHiddenLayers, TransferFunction[] transferFunctions) {
        int[] layers = new int[numInHiddenLayers.length + 2];
        System.arraycopy(numInHiddenLayers, 0, layers, 1, numInHiddenLayers.length);
        layers[0] = numInputs;
        layers[layers.length-1] = numOutputs;

        NeuralNetwork net = new MultiLayerPerceptron(layers);

        // set transfer function for all but first layer
        for(int i = 1; i < net.getLayersCount(); i++) {
            List<Neuron> neuronList = net.getLayerAt(i).getNeurons();
            for (Neuron n : neuronList) {
                n.setTransferFunction(transferFunctions[i-1]);
            }
        }

        net.randomizeWeights();

        return net;
    }

    private void refreshNetworks() {
        for (int i = 0; i < this.numRobots; i++){
            // TODO: reproduction of one or two best nets
//            this.configurations.set(i, this.currentBestConfig.reproduce());
        }
    }

    private int runSim() {
        // Run our specified battle and let it run till it is over
        engine.runBattle(battleSpec, true); // waits till the battle finishes

        return 0;
    }

    public static void main(String[] args) {
        RoboTeacher r = new RoboTeacher(10);
        r.teach();
    }
}
