#!/bin/sh
#
# Copyright (c) 2001-2017 Mathew A. Nelson and Robocode contributors
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://robocode.sourceforge.net/license/epl-v10.html
#

wd=`pwd`
cd "${0%/*}/resources/robocode"
pwd

classpath="libs/robocode.jar:GeneticBot/out/production/GeneticBot/novako19bot/:\
$wd/resources/neuroph-2.94/neuroph-core-2.94.jar:$wd/resources/slf4j-1.7.25/slf4j-api-1.7.25.jar:\
$wd/resources/slf4j-1.7.25/slf4j-simple-1.7.25.jar"

java -Xss515m -DNOSECURITY=true -Xmx512M -cp $classpath -XX:+IgnoreUnrecognizedVMOptions "--add-opens=java.base/sun.net.www.protocol.jar=ALL-UNNAMED" "--add-opens=java.base/java.lang.reflect=ALL-UNNAMED" "--add-opens=java.desktop/sun.awt=ALL-UNNAMED" robocode.Robocode $*
cd "${wd}"
